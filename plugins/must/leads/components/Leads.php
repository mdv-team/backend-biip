<?php

namespace Must\Leads\Components;

use Must\Leads\Models\Lead;
use October\Rain\Support\Facades\Flash;

class Leads extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Leads',
            'description' => 'Receive Contacts Leads informations'
        ];
    }

    // This array becomes available on the page as {{ component.posts }}
    public function onSave()
    {
        $postData = post();
        $mLead = new Lead;

        // make validation        
        $rules = [
            'name' => 'required|string',
            'company' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
            'phone' => 'required|numeric'
        ];        

        // $messages = [
        //     'required' => 'Este campo é obrigatório.',
        //     'string' => 'Formato inválido para este campo.',
        //     'email' => 'Informe um e-mail válido.',
        // ];

        $messages = [
            'name.required' => \Lang::get('must.leads::lang.validations.name_required'),
            'name.string' => \Lang::get('must.leads::lang.validations.name_string'),
            'company.required' => \Lang::get('must.leads::lang.validations.company_required'),
            'company.string' => \Lang::get('must.leads::lang.validations.company_string'),
            'email.required' => \Lang::get('must.leads::lang.validations.email_required'),
            'email.email' => \Lang::get('must.leads::lang.validations.email_email'),
            'phone.required' => \Lang::get('must.leads::lang.validations.phone_required'),
            'phone.regex' => \Lang::get('must.leads::lang.validations.phone_regex'),
            'message.required' => \Lang::get('must.leads::lang.validations.message_required')
        ];        

        $validator = \Validator::make($postData, $rules, $messages);
        if($validator->fails()){
            throw new \ValidationException($validator);
        }else{
            $mLead->name = $postData['name'];
            $mLead->company = $postData['company'];
            $mLead->email = $postData['email'];
            $mLead->message = $postData['message'];
            $mLead->phone = $postData['phone'];
            $mLead->save();
    
            \Flash::success('Dados enviados com sucesso');

            return true;
        }                             
    }
}