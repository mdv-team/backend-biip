<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1010 extends Migration
{
    public function up()
    {
        Schema::create('must_pages_faqs_topics', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('topics');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('must_pages_faqs_topics');
    }
}