<?php namespace Must\Pages;

use Cms\Classes\Theme;
use Lang;
use Must\Pages\Models\Content;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            Components\FormDepends::class => 'formDepends',
            Components\Banners::class => 'banners',
            Components\FAQs::class => 'faqs'           
        ];
    }

    public function registerSettings() {}

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'contenttext' => [$this, 'contentText'],
                'contentimage' => [$this, 'contentImage'],
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'html' => [$this, 'htmlColumn'],
            'imagebanner' => [$this, 'imageBanner'],
            'link' => [$this, 'linkColumn'],
        ];
    }

    public function linkColumn($value, $column, $record)
    {
        return '<a href="' . $value . '" target="_blank" class="link">Acessar URL</a>';
    }

    public function imageBanner($value, $column, $record)
    {
        if ($record->image_desktop) {
            return '<img src="' . $record->image_desktop->getPath() . '" width="140px">';
        }
    }

    public function htmlColumn($value, $column, $record)
    {
        if ('text' == $record->type) {
            return $value;
        } else if ('file' == $record->type) {
            return 'Arquivo';
        } else {
            if ($record->image) {
                return '<img src="' . $record->image->getPath() . '" width="100px">';
            } else {
                return 'Imagem Removida';
            }
        }
    }

    public function contentText($text, $section = '', $removeP = false, $identifier = '', $location = 'Site')
    {
        $theme = Theme::getActiveTheme();        
        $url = $theme->getConfigValue('url');        

        if (!$url) {
            return 'Client URL not set in website config';
        }

        $url = str_replace(["https://", "http://", "/"], "", $url);

        $code = md5($text . $section . $location);

        $content = Content::where('url', $url)->where('hash', $code)->first();

        if (!$content) {
            $content = new Content;
            $content->type = 'text';
            $content->text = $text;
            $content->hash = $code;
            $content->section = $section;
            $content->location = $location;
            $content->identifier = $identifier;
            $content->url = $url;
            $content->url_full = url()->full();
            $content->save();
        } else {
            if ($section != $content->section) {
                $content->section = $section;
                $content->save();
            }

            if ($identifier != $content->identifier) {
                $content->identifier = $identifier;
                $content->save();
            }
        }

        if ($removeP) {
            return str_replace(['<p>', '</p>'], '', $content->text);
        }

        $currentLang = Lang::getLocale();
        return $content->lang($currentLang)->text;
    }
    
    public function contentImage($imgpath, $section = '', $identifier = '', $increment = '', $location = 'Site')
    {
        $theme = Theme::getActiveTheme();        
        $url = $theme->getConfigValue('url');        

        if (!$url) {
            return 'Client URL not set in website config';
        }
        

        $section = trim($section);

        $filePath = $theme->getPath() . '/assets/' . $imgpath;        
        $code = md5($filePath . $section . $increment);

        $filePathRelative = url()->full() . '/themes/' . $theme->getDirName() . '/assets/' . $imgpath;
        // $filePathRelative = '/assets/' . $imgpath;
        $codeRelative = md5($filePathRelative . $section . $increment);        

        if (file_exists($filePath)) {
            
            list($width, $height, $type, $attr) = getimagesize($filePath);

            $content = Content::where('hash', $code)->first();

            if (!$content) {
                $content = new Content;
                $content->type = 'image';
                $content->text = null;
                $content->hash = $code;
                $content->section = $section;
                $content->location = $location;
                $content->identifier = $identifier;
                $content->url = $url;
                $content->url_full = url()->full();       
                $content->hash_relative = $codeRelative;
                $content->image = $filePath;
                $content->width = $width . 'px';
                $content->height = $height . 'px';
                $content->save();

            } else {

                // return $content->code_relative;
                if (empty($content->hash_relative)) {
                    $content->hash_relative = $codeRelative;
                    $content->save();
                }

                if ($section != $content->section) {
                    $content->section = $section;
                    $content->save();
                }

                if ($identifier != $content->identifier) {
                    $content->identifier = $identifier;
                    $content->save();
                }

                if (url()->full() != $content->url) {
                    $content->url = url()->full();
                    $content->save();
                }
            }

            // $detect = new Mobile_Detect;
            // if ($detect->isMobile() || $detect->isTablet()) {
            //     $mobileImage = $content->image_mobile ?? false;
            //     if ($mobileImage) {
            //         return $mobileImage->getPath();
            //     }
            // }
            // echo "<pre>"; var_dump([$filePath, $filePathRelative]); exit;
            // return $content->image;
            return $content->image ? $content->image->getPath() : '';
        }        
    }
}
