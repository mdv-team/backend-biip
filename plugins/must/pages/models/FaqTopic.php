<?php namespace Must\Pages\Models;

use Model;

/**
 * Model
 */
class FaqTopic extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'must_pages_faqs_topics';

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = ['topics'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'faq' => [Faq::class]
    ];

    public function faqs(){
        return $this->hasMany(Faq::class);
    }
}
