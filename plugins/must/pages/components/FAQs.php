<?php namespace Must\Pages\Components;

use Cms\Classes\ComponentBase;
use Must\Pages\Models\Faq;
use Must\Pages\Models\FaqTopic;

class FAQs extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'FAQ Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [
            'limit' => [
                'title' => 'Número de FAQs',
                'description' => 'Permite informar um limite (limit) para a listagem. 0 para ilimitado',
                'default' => 12,
                'type' => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'O limite precisa ser um número',
            ],
            'order' => [
                'title' => 'Ordenação Padrão',
                'description' => 'Permite definir uma ordem padrão',
                'type' => 'dropdown',
                'default' => 'desc',
            ],
        ];
    }

    public function onRun()
    {        
        $this->page['faqs'] = $this->getFAQs();
        $this->page['topics'] = $this->getTopics();
    }

    public function getTopics(){
        $faqModel = new Faq;           
        return $faqModel->topics()->getRelated()->get();
    }

    public function getFAQs()
    {
        $limit = get('limit') ?? $this->property('limit');

        $faqs = Faq::orderBy('id', 'desc')
            ->take($limit)
            ->get();

        return $faqs;
    }
}
