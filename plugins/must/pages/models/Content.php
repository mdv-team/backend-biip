<?php namespace Must\Pages\Models;

use Model;

/**
 * Model
 */
class Content extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = ['text'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'must_pages_contents';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $attachOne = [
        'image' => [
            'System\Models\File',
            'delete' => true,
        ],
    ];

    public function getSectionCompany()
    {
        $user = \BackendAuth::getUser();

        $data = [];

        $content = Content::whereRaw('1=1');
        $content = $content->select([
            \DB::raw('distinct(section) as id'),
            'section',
        ])->get();

        foreach ($content as $key => $v) {
            $data[$v->section] = $v->section;
        }

        return $data;
    }
}
