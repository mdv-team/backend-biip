<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesFaqs extends Migration
{
    public function up()
    {
        Schema::table('must_pages_faqs', function($table)
        {
            $table->integer('id_faq_topic');
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_faqs', function($table)
        {
            $table->dropColumn('id_faq_topic');
        });
    }
}
