<?php namespace Must\Leads\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Leads extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'leads' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Must.Leads', 'leads');
    }
}
