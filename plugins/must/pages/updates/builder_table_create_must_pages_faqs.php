<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMustPagesFaqs extends Migration
{
    public function up()
    {
        Schema::create('must_pages_faqs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('question')->default('null');
            $table->text('answer');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('must_pages_faqs');
    }
}
