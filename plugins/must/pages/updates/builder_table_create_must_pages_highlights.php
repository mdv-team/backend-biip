<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMustPagesHighlights extends Migration
{
    public function up()
    {
        Schema::create('must_pages_highlights', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('must_pages_highlights');
    }
}
