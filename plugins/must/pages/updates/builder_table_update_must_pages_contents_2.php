<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesContents2 extends Migration
{
    public function up()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->string('image')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->dropColumn('image');
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
    }
}
