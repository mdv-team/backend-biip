<?php namespace Must\Pages\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Contents extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'pages.content',
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Must.Pages', 'sites', 'sites-contents');
    }

    public function listFilterExtendScopes($filter)
    {
        $user = \BackendAuth::getUser();

        $scopes = [];

        $scopes['section'] = [
            'label' => 'Páginas',
            'modelClass' => 'Must\Pages\Models\Content',
            'conditions' => 'section in (:filtered)',
            'nameFrom' => 'name',
            'options' => 'getSectionCompany',
        ];

        if (\Schema::hasColumn($this->widget->list->model->table, 'created_at')) {
            $scopes['created_at'] = [
                'label' => 'Criado em',
                'conditions' => 'created_at >= \':after\' AND created_at <= \':before\'',
                'type' => 'daterange',
            ];
        }

        $filter->addScopes($scopes);
    }
}
