$('document').ready(function () {
  const $win = $(window);
  const $main = $('main');
  const $body = $('body');
  const $backdrop = $('.backdrop__bar');
  const $btnMenu = $('.btn-menu');
  const $menuItem = $('.aside-menu')
  const $contentHeader = $('.content-header');
  const $btnAbout = $('.btn-about');

  const isScrolled = function () {
    return $win.scrollTop() > 200;
  };
  const isDesktop = function () {
    return $win.outerWidth() >= 768;
  };
  function scrollToContent(element, value) {
    $('html, body').animate({
      scrollTop: $('.' + element).offset().top - value
    }, 800);
  }

  /* SLIDERS DEFAULT */
  const swiperImages = new Swiper('.swiper-images', {
    direction: 'horizontal',
    loop: false,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    speed: 600,
    slidesPerView: 3,
    autoplay: {
      delay: 5000,
    },
    clickable: true,
    breakpoints: {
      991: {
        slidesPerView: 2.2,
        slidesPerGroup: 1,
        loop: true,
      }
    },
    navigation: {
      nextEl: '.swiper-button-next-images',
      prevEl: '.swiper-button-prev-images',
    },
  });
  const swiperFaqs = new Swiper('.swiper-faqs', {
    direction: 'horizontal',
    loop: false,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    speed: 600,
    slidesPerView: 1,
    autoplay: {
      delay: 5000,
    },
    clickable: true,
    navigation: {
      nextEl: '.swiper-button-next-faqs',
      prevEl: '.swiper-button-prev-faqs',
    },
  });
  /* *************** */

  /* FUNCTION DEFAULT */
  var itemOpen = function () {
    $backdrop.removeClass('d-none');
    $body.addClass('modal-open');
  }
  var itemClose = function () {
    $backdrop.addClass('d-none');
    $body.removeClass('modal-open');
  };
  var menuOpen = function () {
    $menuItem.addClass('aside-menu-active');
    itemOpen();
  }
  var menuClose = function () {
    $menuItem.removeClass('aside-menu-active');
    itemClose();
  }

  // var headerScrolledOpen = function () {
  //   $contentHeader.addClass('content-header-scrolled');
  // }
  // var headerScrolledClose = function () {
  //   $contentHeader.removeClass('content-header-scrolled');
  // }
  /* ****************** */

  $btnMenu.click(function () {
    if ($menuItem.hasClass('aside-menu-active')) {
      $btnMenu.find('span').removeClass('active');
      $btnMenu.find('p').text('Menu');
      menuClose();
    }
    else {
      $btnMenu.find('span').addClass('active');
      $btnMenu.find('p').text('Fechar');
      menuOpen();
    }
  });


  /* BUTTON SCROLABLE */
  $btnAbout.click(function () {
    if (isDesktop()) {
      scrollToContent('content-home-details', 70);
    }
    else {
      scrollToContent('content-home-details', 30);
    }
    $btnMenu.find('span').removeClass('active');
    $btnMenu.find('p').text('Menu');
    menuClose();
  });
  if (window.location.hash == "#linkAbout") {
    if (isDesktop()) {
      scrollToContent('content-home-details', 70);
    }
    else {
      scrollToContent('content-home-details', 30);
    }
    $btnMenu.find('span').removeClass('active');
    $btnMenu.find('p').text('Menu');
    menuClose();
  }
  /* *********************** */

  // AOS.init({
  //   offset: 0,
  //   delay: 0,
  //   duration: 950,
  //   easing: 'ease-in-sine',
  // });
});


