<?php namespace Must\Pages\Models;

use Model;

/**
 * Model
 */
class Faq extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'must_pages_faqs';

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = ['question', 'answer'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'topico' => [FaqTopic::class, 'key' => 'id_faq_topic']
    ];

    public function getIdFaqTopicOptions() {
        return FaqTopic::lists('topics','id');
    }

    public function topics(){
        return $this->belongsTo(FaqTopic::class);
    }
}
